import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import firebase from 'firebase'
// Required for side-effects
require("firebase/firestore")

Vue.config.productionTip = false
// Initialize Firebase
const config = {
  apiKey: "AIzaSyBwgUT2SS-0iUg-F2FrURziyDbKT1toEWw",
  authDomain: "chattest-4c84b.firebaseapp.com",
  databaseURL: "https://chattest-4c84b.firebaseio.com",
  projectId: "chattest-4c84b",
  storageBucket: "",
  messagingSenderId: "148379460115"
}
firebase.initializeApp(config)

// Initialize Cloud Firestore through Firebase
var db =firebase.firestore()
// Disable deprecated features
window.db  = db
db.settings({
  timestampsInSnapshots: true
})



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
